#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from flask import Flask, render_template, request
from keras.models import load_model
from PIL import Image
from io import BytesIO, StringIO
import numpy as np
import io
import scipy.misc
import pickle

app = Flask(__name__)

UPLOAD_FOLDER = '/static/image/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

model = load_model('./static/model/da_last4_layers.h5')

@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload_file():
    photo = request.files['image2'].stream

    # Convert to be read by keras / tensorflow
    size = 150, 150
	
    img = Image.open(photo)
    img = img.resize(size, Image.ANTIALIAS)
    # img.save(UPLOAD_FOLDER + "test.jpg", "JPEG")
    pix = np.array(img)
	
    pix = pix.reshape((1, 150, 150, 3))

    predictions = model.predict(pix)
    prediction = np.argmax(predictions)

    class_predictions = {0:"Juji-gatame", 1:"Kimura", 2:"Triangle"}
	
	# --------------- TO DELETE --------------- 
    # prediction = 0
	# --------------- TO DELETE --------------- 
    pix = pix.reshape((150, 150, 3))
	
    # imwrite(f, img )

    return render_template('picture_result.html', pred = class_predictions[prediction], imgpath = "test.jpg")
	
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
    
